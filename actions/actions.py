# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
from rasa_sdk.events import SlotSet
from forex_python.converter import CurrencyRates
from datetime import datetime
import requests

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []


class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_hello_world"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="Hello World!")

        return []


class ActionGetExchangeRate(Action):

    def name(self) -> Text:
        return "action_get_exchange_rate"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        buy_currency = tracker.get_slot("buy_currency")
        sell_currency= tracker.get_slot("sell_currency")
        amount  = tracker.get_slot("data")
        print ("AMOUNT: " + str(amount))
        print( "BUY SELL: " + str(buy_currency) + " " + str(sell_currency))
        c= CurrencyRates()
        ex_rate = c.get_rate(buy_currency.upper(), sell_currency.upper())
        d= datetime.today().strftime("%m/%d/%Y")

        # base_url = "https://www.alphavantage.co/query"
        # api_key = "L7FIGPZ8BODPN3BA"

        # # parameters
        # PARAMS = {'function': "CURRENCY_EXCHANGE_RATE", 'from_currency': from_currency,'to_currency': to_currency, 'apikey': api_key}

        # main_url = base_url + "&from_currency =" + from_currency + \
        #     "&to_currency =" + to_currency + "&apikey =" + api_key
        
        # req_ob = requests.get(url=base_url, params=PARAMS)
        # result = req_ob.json()
        # print (result.get("5. Exchange_rate"))

        dispatcher.utter_message(text="The exchnage rate is " + str(ex_rate) + " on date: " + str(d))

    